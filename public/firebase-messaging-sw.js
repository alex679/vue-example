importScripts('https://www.gstatic.com/firebasejs/7.12.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.12.0/firebase-messaging.js');

firebase.initializeApp({
  apiKey: 'AIzaSyAtHiJw-mepzPYpslQ8uf2vWaEeScG4WvI',
  authDomain: 'vue-examplelife-37d73.firebaseapp.com',
  databaseURL: 'https://vue-examplelife-37d73.firebaseio.com',
  projectId: 'vue-examplelife-37d73',
  storageBucket: 'vue-examplelife-37d73.appspot.com',
  messagingSenderId: '569334975679',
  appId: '1:569334975679:web:11c1016e2f1c1a2e960a0a',
});

if (firebase.messaging.isSupported()) {
  const messaging = firebase.messaging();

  messaging.setBackgroundMessageHandler((payload) => {
    const notification = JSON.parse(payload.data.notification);
    const notificationTitle = notification.title;
    const notificationOptions = {
      body: notification.body,
    };

    return self.registration.showNotification(
      notificationTitle,
      notificationOptions,
    );
  });

  self.addEventListener('notificationclick', (event) => {
    const target = event.notification.data.click_action || '/';
    event.notification.close();

    // этот код должен проверять список открытых вкладок и переключатся на открытую
    // вкладку с ссылкой если такая есть, иначе открывает новую вкладку
    event.waitUntil(clients.matchAll({
      type: 'window',
      includeUncontrolled: true,
    })
      .then((clientList) => {
        // clientList почему-то всегда пуст!?
        for (let i = 0; i < clientList.length; i++) {
          const client = clientList[i];
          if (client.url == target && 'focus' in client) {
            return client.focus();
          }
        }
        return clients.openWindow(target);
      }));
  });
}
