const getters = {
  // user
  token: state => state.user.token,
  loggedUser: state => state.user.user,
  userType: state => state.user.user.type,
  isGirl: state => state.user.user,// && state.user.user.type === 'girl',
  isManager: state => state.user.user && state.user.user.type === 'manager',
  phoneVerified: state => state.user.user && state.user.user.phone_verified === 'yes',
  profileIsVerified: () => true,
  isActive: state => state.user.user && state.user.user.status === 'active',
  isSubscriptionPayed: state => state.user.user && state.user.user.subscription_payed,
  isBlocked: state => state.user.user && state.user.user.status === 'blocked',
  userMedias: state => state.user.userMedias,

  // offers
  offers: state => state.offers.offers.filter(offer => !Array.isArray(offer)),
  currentOffer: state => state.offers.currentOffer,
  currentOfferGoingUsers: state => {
    return !state.offers.currentOffer.users_going ? '' : state.offers.currentOffer.users_going.filter((user) => user.id !== state.offers.currentOffer.manager.id)
  },


  // tours
  tours: state => state.tours.tours,
  currentTour: state => state.tours.currentTour,

  // messages
  dialogs: state => state.messages.dialogs,
  currentDialog: state => state.messages.currentDialog,
  messages: state => state.messages.messages,

  // options
  countries: state => state.options.countries,
  hairs: state => state.options.hairs,
  eyes: state => state.options.eyes,
  nationalities: state => state.options.nationalities,
  visas: state => state.options.visas,
  categories: state => state.options.categories,
  locale: state => state.options.locale,
};
export default getters;
