import {
  createTour,
  fetchMyTours,
  fetchTours,
  fetchTour,
  fetchTourForModel,
  deleteTour,
  closeTour,
} from '@/api/tours';

const state = {
  tours: [],
  currentTour: {},
};

const mutations = {
  SET_TOURS: (state, tours) => {
    state.tours = tours;
  },
  APPEND_TOUR: (state, tour) => {
    state.tours.push(tour);
  },
  PREPEND_TOUR: (state, tour) => {
    state.tours.splice(0, 0, tour);
  },
  CLEAR_TOURS: (state) => {
    state.tours = [];
  },
  SET_CURRENT_TOUR: (state, tour) => {
    state.currentTour = tour;
  },
  DELETE_TOUR: (state, tour_id) => {
    const offerIndex = state.tours.findIndex(el => el.id === tour_id);
    if (offerIndex !== -1) {
      state.tours.splice(offerIndex, 1);
    }
  },
  SET_TOUR_STATUS: (state, { tour_id, status }) => {
    if (state.currentTour && state.currentTour.id === tour_id) {
      state.currentTour.status = status;
    }
    const tourIndex = state.tours.findIndex(el => el.id === tour_id);
    if (tourIndex !== -1) {
      state.tours[tourIndex].status = status;
    }
  },
};

const actions = {
  async createTour({ commit }, data) {
    return new Promise((resolve, reject) => {
      createTour(data)
        .then((response) => {
          commit('PREPEND_TOUR', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async loadMyTours({ commit }) {
    return new Promise((resolve, reject) => {
      fetchMyTours()
        .then((response) => {
          commit('SET_TOURS', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async loadTours({ commit }) {
    return new Promise((resolve, reject) => {
      fetchTours()
        .then((response) => {
          commit('SET_TOURS', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async loadMyTour({ commit }, id) {
    return new Promise((resolve, reject) => {
      fetchTour(id)
        .then((response) => {
          commit('SET_CURRENT_TOUR', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async loadModelTour({ commit }, id) {
    return new Promise((resolve, reject) => {
      fetchTourForModel(id)
        .then((response) => {
          commit('SET_CURRENT_TOUR', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async removeTour({ commit }, id) {
    return new Promise((resolve, reject) => {
      deleteTour(id)
        .then((response) => {
          commit('DELETE_TOUR', id);
          resolve();
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async closeTour({ commit }, tour_id) {
    return new Promise((resolve, reject) => {
      closeTour(tour_id)
        .then((response) => {
          commit('SET_TOUR_STATUS', { tour_id, status: 'closed' });
          resolve();
        }).catch((error) => {
          reject(error);
        });
    });
  },

  changeTourStatus({ commit }, { tour_id, status }) {
    commit('SET_TOUR_STATUS', { tour_id, status });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};

