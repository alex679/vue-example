import { login, fetchMe, register, validateVerifyTokenFirebase, validateVerifyCallCode } from '@/api/auth';
import { getToken, setToken, removeToken } from '@/utils/auth';
import {
  deleteUser,
  removePhoto,
  uploadPhoto,
  uploadVideo,
  updateUser,
  uploadVerifyPhoto,
  addPushToken,
  removePushToken, sendVerifyRequest,
} from '@/api/users';
import Vue from 'vue';

/* eslint-disable-next-line */
const state = {
  token: getToken(),
  user: null,
  userMedias: [],
};

const mutations = {
  SET_USER_DATA: (state, user) => {
    state.user = user;

    if (user && user.photos) {
      const photos = user.photos.map((photo) => {
        photo.url = photo.big;
        photo.type = 'image';
        return photo;
      });

      const videos = user.videos.map((video) => {
        video.type = 'video';
        return video;
      });

      state.userMedias = [...photos, ...videos];
    }
  },
  SET_PHONE_VERIFIED: (state, status) => {
    state.user.phone_verified = status;
  },

  SET_TOKEN: (state, token) => {
    state.token = token;
  },

  APPEND_PHOTO: (state, image) => {
    state.userMedias.push(Object.assign(image, {
      url: image.big,
      type: 'image',
    }));
    if (image.avatar) { state.user.avatar = image; }
  },

  APPEND_VIDEO: (state, video) => {
    state.userMedias.push(Object.assign(video, {
      type: 'video',
    }));
  },

  SET_VERIFIED: (state, status) => {
    state.user.verified = status;
  },

  SET_NOT_VERIFIED: (state, reason) => {
    state.user.verified = 'not';
    state.user.unverified_reason = reason;
  },

  SET_SUBSCRIPTION_PAYED_STATUS: (state, status) => {
    state.user.subscription_payed = status;
  },

  UPDATE_USER: (state, user) => {
    state.user = Object.assign({}, state.user, user);
  },

  SET_CAN_CREATE_OFFERS: (state) => {
    state.user.can_create_offers = 1;
  },

  SET_CAN_CREATE_TOURS: (state) => {
    state.user.can_create_tours = 1;
  },

  SET_MEDIA_CHECKED: (state, mediaId) => {
    const mediaIndex = state.userMedias.findIndex(media => media.id === mediaId);
    if (mediaIndex !== -1) { Vue.set(state.userMedias[mediaIndex], 'checked', true); }
  },

  REMOVE_MEDIA_USER: (state, mediaId) => {
    const mediaIndex = state.userMedias.findIndex(media => media.id === mediaId);
    const media = state.userMedias[mediaIndex];
    if (media.avatar) {
      state.user.avatar = null;
    }
    state.userMedias.splice(mediaIndex, 1);
  },
};

const actions = {
  // user login
  async login({ commit }, userInfo) {
    const { phone, password } = userInfo;
    return new Promise((resolve, reject) => {
      login({ phone: phone.trim(), password })
        .then((response) => {
          commit('SET_USER_DATA', response.data);
          commit('SET_TOKEN', response.data.token);
          setToken(response.data.token);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  // user register
  async register({ commit }, data) {
    return new Promise((resolve, reject) => {
      register(data)
        .then((response) => {
          commit('SET_USER_DATA', response.data);
          commit('SET_TOKEN', response.data.token);
          setToken(response.data.token);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  // get user info
  async getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      fetchMe().then((response) => {
        const { data } = response;
        commit('SET_USER_DATA', data);
        resolve(data);
      }).catch((error) => {
        reject(error);
      });
    });
  },

  // user logout
  async logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      //      logout(state.token).then(() => {
      commit('SET_TOKEN', null);
      commit('SET_USER_DATA', null);
      removeToken();
      resolve();
      //      }).catch(error => {
      //        reject(error)
      //      })
    });
  },

  validateVerifyTokenFirebase({ commit }, token) {
    return new Promise((resolve, reject) => {
      validateVerifyTokenFirebase(token)
        .then((response) => {
          if (response.success) { commit('SET_PHONE_VERIFIED', 'yes'); }
          resolve(response);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  validateVerifyCallCode({ commit }, code) {
    return new Promise((resolve, reject) => {
      validateVerifyCallCode(code)
        .then((response) => {
          if (response.success) { commit('SET_PHONE_VERIFIED', 'yes'); }
          resolve(response);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  // remove token
  resetToken({ commit }) {
    return new Promise((resolve) => {
      commit('SET_TOKEN', '');
      removeToken();
      resolve();
    });
  },

  async deleteProfile({ commit, rootGetters }, reason = '') {
    const user_id = rootGetters.loggedUser.id;
    return new Promise((resolve, reject) => {
      deleteUser(user_id, reason)
        .then((response) => {
          commit('SET_TOKEN', null);
          commit('SET_USER_DATA', null);
          removeToken();
          resolve();
          resolve(response);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async updateUser({ commit, rootGetters }, data) {
    const user_id = rootGetters.loggedUser.id;

    return new Promise((resolve, reject) => {
      updateUser(user_id, data)
        .then((resp) => {
          fetchMe().then((response) => {
            const { data } = response;
            commit('UPDATE_USER', data);
            resolve(data);
          });
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async uploadPhoto({ commit, rootGetters }, file) {
    const user_id = rootGetters.loggedUser.id;

    return new Promise((resolve, reject) => {
      uploadPhoto(user_id, file)
        .then((response) => {
          commit('APPEND_PHOTO', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async uploadVideo({ commit, rootGetters }, file) {
    const user_id = rootGetters.loggedUser.id;

    return new Promise((resolve, reject) => {
      uploadVideo(user_id, file)
        .then((response) => {
          commit('APPEND_VIDEO', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async uploadVerifyPhoto({ commit, rootGetters }, file) {
    const user_id = rootGetters.loggedUser.id;

    return new Promise((resolve, reject) => {
      uploadVerifyPhoto(user_id, file)
        .then((response) => {
          commit('SET_VERIFIED', 'wait');
          resolve(response);
        }).catch((error) => {
          reject(error);
        });
    });
  },


  async removeMedia({ commit, rootGetters }, id) {
    const user_id = rootGetters.loggedUser.id;

    return new Promise((resolve, reject) => {
      removePhoto(user_id, id)
        .then((response) => {
          commit('REMOVE_MEDIA_USER', id);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async addPushToken({ commit, rootGetters }, token) {
    const user_id = rootGetters.loggedUser.id;
    return new Promise((resolve, reject) => {
      addPushToken(user_id, { token, os: 'web' })
        .then((response) => {
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async removePushToken({ commit, rootGetters }, token) {
    const user_id = rootGetters.loggedUser.id;
    return new Promise((resolve, reject) => {
      removePushToken(user_id, { token, os: 'web' })
        .then((response) => {
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async sendVerifyRequest({ commit, rootGetters }) {
    const user_id = rootGetters.loggedUser.id;
    return new Promise((resolve, reject) => {
      sendVerifyRequest(user_id)
        .then((response) => {
          commit('SET_VERIFIED', 'wait');
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  setMediaChecked({ commit }, media_id) {
    commit('SET_MEDIA_CHECKED', media_id);
  },

  setVerified({ commit }) {
    commit('SET_VERIFIED', 'yes');
  },

  setNotVerified({ commit }, reason = '') {
    commit('SET_NOT_VERIFIED', reason);
  },

  setUserCanCreateTours({ commit }) {
    commit('SET_CAN_CREATE_TOURS');
  },

  setUserCanCreateOffers({ commit }) {
    commit('SET_CAN_CREATE_OFFERS');
  },

  setSubscriptionPayedStatus({ commit }, status = true) {
    commit('SET_SUBSCRIPTION_PAYED_STATUS', status);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
