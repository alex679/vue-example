import { fetchCountries } from '@/api/geo';
import { getQuestions } from "@/api/faq";
import { fetchMainOptions, fetchOffersCategories } from '@/api/options';
import { getLocale, setLocale } from '@/utils/locale';

const state = {
  countries: [],
  hairs: [],
  eyes: [],
  nationalities: [],
  visas: [],
  categories: [],
  locale: getLocale() || 'ru',
};

const mutations = {
  SET_COUNTRIES: (state, countries) => {
    state.countries = countries;
  },
  SET_MAIN_OPTIONS: (state, options) => {
    state.eyes = options.eyes;
    state.hairs = options.hairs;
    state.nationalities = options.nationalities;
    state.visas = options.visas;
  },
  SET_OFFER_CATEGORIES: (state, data) => {
    state.categories = data;
  },
};

const actions = {
  async fetchCountries({ commit }) {
    return new Promise((resolve, reject) => {
      fetchCountries()
        .then((response) => {
          commit('SET_COUNTRIES', response.data);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async fetchMainOptions({ commit }) {
    return new Promise((resolve, reject) => {
      fetchMainOptions()
        .then((response) => {
          commit('SET_MAIN_OPTIONS', response);
          resolve();
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async fetchFaqQuestions() {
    return new Promise((resolve, reject) => {
      getQuestions()
        .then((response) => {
          resolve(response)
        })
        .catch(e => {
          reject(e)
        })
    })
  },

  async fetchOffersCategories({ commit }) {
    return new Promise((resolve, reject) => {
      fetchOffersCategories()
        .then((response) => {
          commit('SET_OFFER_CATEGORIES', response.data);
          resolve();
        }).catch((error) => {
          reject(error);
        });
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};

