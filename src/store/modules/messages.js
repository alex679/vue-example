import {
  fetchDialogs,
  sendMessage,
  sendMessageByDialog,
  markAsRead,
  markDialogAsRead,
  fetchMessages,
} from '@/api/chat';
import { Notification } from 'element-ui';

function mapMessage(message, user_id) {
  message.from_me = message.user_from_id == user_id;
  return message;
}

function mapDialog(dialog, user_id) {
  if (!dialog.userFirst) { return dialog; }

  const recipient = dialog.userFirst.id == user_id ? dialog.userSecond : dialog.userFirst;
  if (recipient && recipient.avatar) {
    dialog.avatar = recipient.avatar;
  } else {
    dialog.avatar = null;
  }
  dialog.recipient = recipient;

  if (dialog.lastMessage) {
    dialog.lastMessage = mapMessage(dialog.lastMessage, user_id);
  }

  dialog.users = dialog.users;
  dialog.title = dialog.title;

  return dialog;
}

const state = {
  dialogs: [],
  currentDialog: {},
  messages: [],
};

const mutations = {
  SET_DIALOGS: (state, dialogs) => {
    state.dialogs = dialogs;
  },

  PREPEND_DIALOG: (state, dialog) => {
    const dialogIndex = state.dialogs.findIndex(
      el => el.id === dialog.id
          || dialog.recipient && el.id === `tmp_dialog_with_${dialog.recipient.id}`
          || el.recipient && `tmp_dialog_with_${el.recipient.id}` === dialog.id,
    );
    if (dialogIndex == -1) {
      // вставляем диалог в начало списка
      state.dialogs.unshift(dialog);
    } else {
      // проставляем корректный dialog_id (если приходит dialog через сокет, то в нем корректный dialog_id)
      const findedDialog = state.dialogs[dialogIndex];
      if (String(dialog.id).indexOf('tmp_dialog_with_') === -1 && String(findedDialog.id).indexOf('tmp_dialog_with_') !== -1) {
        state.dialogs[dialogIndex].id = dialog.id;
      }
    }
  },

  SET_CURRENT_DIALOG: (state, dialog) => {
    state.currentDialog = dialog;
  },

  SET_MESSAGES: (state, messages) => {
    state.messages = messages;
  },

  APPEND_MESSAGE: (state, message) => {
    if (message.dialog_id === state.currentDialog.id) {
      if(state.messages.find((stateMessage) => stateMessage.id == message.id) !== undefined) {
        return;
      }
      state.messages.push(message);
    }
    const dialogIndex = state.dialogs.findIndex(el => el.id === message.dialog_id);
    if (dialogIndex !== -1) {
      const dialog = state.dialogs[dialogIndex];
      dialog.lastMessage = message;
      dialog.last_message_time = message.created_at;
      // обновляем позицию дилога
      state.dialogs.splice(dialogIndex, 1);
      state.dialogs.unshift(dialog);
    }
  },

  RESET_MESSAGES: (state) => {
    state.messages = [];
  },

  MARK_AS_READ: (state, message_id) => {
    const message_index = state.messages.findIndex(el => el.id === message_id);
    if (message_index !== -1) {
      state.messages[message_index].read = 1;
    }

    // ищем сообщение в диалогах
    const dialog_index = state.dialogs.findIndex(el => el.lastMessage && el.lastMessage.id === message_id);
    if (dialog_index !== -1) { state.dialogs[dialog_index].lastMessage.read = 1; }
  },
  MARK_DIALOG_AS_READ: (state, dialog_id) => {
    const dialog_index = state.dialogs.findIndex(el => el.id === dialog_id);
    if (dialog_index !== -1) { state.dialogs[dialog_index].readed = 1; }
  },
};

const actions = {
  async fetchDialogs({ state, commit, rootGetters }) {
    const user_id = rootGetters.loggedUser.id;
    return new Promise((resolve, reject) => {
      fetchDialogs()
        .then((response) => {
          const dialogs = response.data;
          dialogs.map(dialog => mapDialog(dialog, user_id))
            .filter(dialog => !!dialog.recipient);
          commit('SET_DIALOGS', dialogs);

          if(state.currentDialog) {
            const currentDialog = dialogs.find((dialog) => dialog.id == state.currentDialog.id)
            if(currentDialog) {
              commit('SET_CURRENT_DIALOG', currentDialog);
            }
          }

          resolve(dialogs);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async sendMessage({ commit }, data) {
    return new Promise((resolve, reject) => {
      sendMessage(data)
        .then((response) => {
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async sendMessageByDialog({ commit }, data) {
    return new Promise((resolve, reject) => {
      sendMessageByDialog(data)
        .then((response) => {
          resolve(response.data);
        }).catch((error) => {
        reject(error);
      });
    });
  },

  async markMessageAsRead({ commit }, id) {
    return new Promise((resolve, reject) => {
      markAsRead(id)
        .then((response) => {
          commit('MARK_AS_READ', id);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async markDialogAsRead({ commit }, id) {
    return new Promise((resolve, reject) => {
      markDialogAsRead(id)
        .then((response) => {
          commit('MARK_DIALOG_AS_READ', id);
          resolve(response.data);
        }).catch((error) => {
        reject(error);
      });
    });
  },

  async fetchMessages({ commit, rootGetters }, dialog_id) {
    const user_id = rootGetters.loggedUser.id;
    return new Promise((resolve, reject) => {
      fetchMessages(dialog_id)
        .then((response) => {
          const messages = response.data.reverse();
          messages.map(message => mapMessage(message, user_id));
          commit('SET_MESSAGES', messages);
          resolve(response.data);
        }).catch((error) => {
          reject(error);
        });
    });
  },

  async appendNewMessage({ commit, rootGetters, dispatch }, { message, dialog }) {
    const user_id = rootGetters.loggedUser.id;

    // если это новый диалог
    if (dialog) {
      dialog = mapDialog(dialog, user_id);
      commit('PREPEND_DIALOG', dialog);
    }

    message = mapMessage(message, user_id);
    commit('APPEND_MESSAGE', message);

    // если это текущий открытый диалог, то проставляем его прочитанным
    if (state.currentDialog.id == message.dialog_id && !message.from_me) {
      dispatch('markDialogAsRead', dialog.id);
    }

    // если не открыт данный диалог
    if (!message.from_me && (!state.currentDialog || state.currentDialog.id != message.dialog_id)) {
      Notification.success('Новое сообщение!');
      //      playNotificationSound()
    }
  },

  setCurrentDialog({ commit }, dialog) {
    commit('SET_CURRENT_DIALOG', dialog);
  },

  resetMessages({ commit }) {
    commit('RESET_MESSAGES');
  },

};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
