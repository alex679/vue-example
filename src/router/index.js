import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/layout';

Vue.use(Router);

export const constantRoutes = [
  {
    path: '/',
    name: 'Offers',
    component: Layout,
    redirect: '/offers',
    children: [
      {
        path: '/offers',
        component: () => import('@/pages/offers/index'),
        name: 'Offers',
        meta: {
          title: 'Offers',
          category_id: 1
        },
      },
      /*{
        path: '/journeys',
        component: () => import('@/pages/offers/index'),
        name: 'Journeys',
        meta: {
          title: 'Путешествия',
          category_id: 2
        },
      },*/
      {
        path: '/offers/my',
        component: () => import('@/pages/offers/my/index'),
        name: 'Offers_my',
        meta: { title: 'My offers' },
      },
      {
        path: '/offers/:id(\\d+)',
        component: () => import('@/pages/offers/inner'),
        name: 'Offer',
        meta: { title: 'Offer' },
      },
    ],
  },
  {
    path: '/tours',
    name: 'Tours',
    component: Layout,
    redirect: '/tours/index',
    children: [
      {
        path: '/tours/index',
        name: 'Tours',
        component: () => import('@/pages/tours/index'),
        meta: { title: 'Tours' },
      },
    ],
  },
  {
    path: '/messages',
    name: 'Messages',
    component: Layout,
    redirect: '/messages/index',
    children: [
      {
        path: '/messages/index',
        name: 'Messages',
        component: () => import('@/pages/messages/index'),
        meta: { title: 'Messages' },
      },
    ],
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Layout,
    redirect: '/settings/index',
    children: [
      {
        path: '/settings/index',
        name: 'Settings',
        component: () => import('@/pages/settings/index'),
        meta: { title: 'Settings' },
      },
      {
        path: '/settings/profile',
        name: 'Profile',
        component: () => import('@/pages/settings/profile'),
        meta: { title: 'Profile' },
      },
      {
        path: '/settings/anceta',
        name: 'Anceta',
        component: () => import('@/pages/settings/anceta'),
        meta: { title: 'Profile info' },
      },
    ],
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/pages/login/index'),
    meta: { title: 'Login' },
    hidden: true,
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/pages/login/register'),
    meta: { title: 'Registration' },
    hidden: true,
  },
  {
    path: '/auth-token',
    name: 'AuthToken',
    component: () => import('@/pages/login/auth-token'),
    meta: { title: 'Entering' },
    hidden: true,
  },
];


const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes,
});

const router = createRouter();

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
