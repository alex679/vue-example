import request from '@/utils/request';

export async function getLatestVersionApp(os, version) {
  return new Promise((resolve, reject) => {
    request.get('/version', { params: { os, version } })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchOffersCategories() {
  return new Promise((resolve, reject) => {
    request.get('/offers/categories')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchEyes() {
  return new Promise((resolve, reject) => {
    request.get('/eyes')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchNationalities() {
  return new Promise((resolve, reject) => {
    request.get('/nationalities')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchPurposes() {
  return new Promise((resolve, reject) => {
    request.get('/purposes')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchHairs() {
  return new Promise((resolve, reject) => {
    request.get('/hairs')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchMainOptions() {
  return new Promise((resolve, reject) => {
    request.get('/options')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function getGirlByHashLink(alias) {
  return new Promise((resolve, reject) => {
    request.get(`/links/${alias}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
