import request from '@/utils/request';

export async function fetchReferrals() {
  return new Promise((resolve, reject) => {
    request.get('/referrals')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchStats() {
  return new Promise((resolve, reject) => {
    request.get('/referrals/stats')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
