import request from '@/utils/request';

export async function fetchUsers(query) {
  return new Promise((resolve, reject) => {
    request.get('/users', { params: query })
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function uploadPhoto(userId, file) {
  const formData = new FormData();
  formData.append('file', file);

  return new Promise((resolve, reject) => {
    request.post(`/users/${userId}/photos`, formData)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function uploadVideo(userId, file) {
  const formData = new FormData();
  formData.append('file', file);

  return new Promise((resolve, reject) => {
    request.post(`/users/${userId}/videos`, formData)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function uploadVerifyPhoto(userId, file) {
  const formData = new FormData();
  formData.append('file', file);

  return new Promise((resolve, reject) => {
    request.post(`/users/${userId}/uploadVerifyPhoto`, formData)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function removePhoto(userId, id) {
  return new Promise((resolve, reject) => {
    request.post(`/users/${userId}/removePhoto`, { id })
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function setAvatar(data) {
  return new Promise((resolve, reject) => {
    request.post(`/users/${data.id}/setAvatar`, { id: data.media_id })
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function updateUser(userId, data) {
  return new Promise((resolve, reject) => {
    request.put(`/users/${userId}`, data)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function fetchUser(id) {
  return new Promise((resolve, reject) => {
    request.get(`/users/${id}`)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function addPushToken(id, data) {
  return new Promise((resolve, reject) => {
    request.post(`/users/${id}/addPushToken`, data)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function removePushToken(id, data) {
  return new Promise((resolve, reject) => {
    request.post(`/users/${id}/removePushToken`, data)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function setVerified(id) {
  return new Promise((resolve, reject) => {
    request.post(`/users/${id}/setVerified`)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function sendVerifyRequest(id) {
  return new Promise((resolve, reject) => {
    request.post(`/users/${id}/sendVerifyRequest`)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function giveMonthAccessUser(id) {
  return new Promise((resolve, reject) => {
    request.post(`/users/${id}/extend`)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function getLink(id) {
  return new Promise((resolve, reject) => {
    request.get(`/users/${id}/link`)
      .then((res) => {
        resolve(res.data.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export async function deleteUser(id, reason = '') {
  return new Promise((resolve, reject) => {
    request.delete(`/users/${id}`, { params: { reason } })
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
