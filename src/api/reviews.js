import request from '@/utils/request';

export async function fetchUserReviews(id) {
  return new Promise((resolve, reject) => {
    request.get(`/users/${id}/reviews`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function createReview(data) {
  return new Promise((resolve, reject) => {
    request.post('/reviews', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
