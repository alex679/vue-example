import request from '@/utils/request';

export async function register(data) {
  return new Promise((resolve, reject) => {
    request.post('/auth/register', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function login(data) {
  return new Promise((resolve, reject) => {
    request.post('/auth/login', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchMe() {
  return new Promise((resolve, reject) => {
    request.get('/auth/me')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function sendVerifyCode() {
  return new Promise((resolve, reject) => {
    request.post('/verify/send')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function validateVerifyCode(code) {
  return new Promise((resolve, reject) => {
    request.post('/verify/verify', { code })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function validateVerifyTokenFirebase(token) {
  return new Promise((resolve, reject) => {
    request.post('/verify/verifyByFirebase', { token })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function sendCallCode() {
  return new Promise((resolve, reject) => {
    request.post('/verify/sendCall')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function validateVerifyCallCode(code) {
  return new Promise((resolve, reject) => {
    request.post('/verify/verifyCall', { code })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function resetPasswordSendCode(phone) {
  return new Promise((resolve, reject) => {
    request.post('/auth/reset/send', { phone })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function resetPasswordChange(data) {
  return new Promise((resolve, reject) => {
    request.post('/auth/reset/verify', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function getSubscriptionBill() {
  return new Promise((resolve, reject) => {
    request.get('/profile/getSubscriptionBill')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function setNotificationsSettings(data) {
  return new Promise((resolve, reject) => {
    request.post('/profile/notificationsChange', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function changePassword(data) {
  return new Promise((resolve, reject) => {
    request.post('/profile/changePassword', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function addToBlackList(id) {
  return new Promise((resolve, reject) => {
    request.post('/profile/addToBlackList', { id })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function removeFromBlackList(id) {
  return new Promise((resolve, reject) => {
    request.post('/profile/removeFromBlackList', { id })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
