import request from '@/utils/request';

export async function fetchCountries(query = {}) {
  return new Promise((resolve, reject) => {
    request.get('/countries', query)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchCitiesForCountries(countryId) {
  return new Promise((resolve, reject) => {
    request.get('/cities', { params: { country_id: countryId } })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchCities(params = {}) {
	return new Promise((resolve, reject) => {
	  request.get('/cities', {params: params})
		.then((response) => {
		  resolve(response.data);
		})
		.catch((exception) => {
		  reject(exception);
		});
	});
}


export async function fetchCitiesTop() {
  return new Promise((resolve, reject) => {
    request.get('/cities/top')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
