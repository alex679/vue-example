import request from '@/utils/request';

export async function fetchMyOffers(params) {
  return new Promise((resolve, reject) => {
    request.get('/offers/my', { params })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchAllOffers(params) {
  return new Promise((resolve, reject) => {
    request.get('/offers/all', { params })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchOffers(params) {
  return new Promise((resolve, reject) => {
    request.get('/offers', { params })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchOffer(id) {
  return new Promise((resolve, reject) => {
    request.get(`/offers/${id}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchOfferForModel(id) {
  return new Promise((resolve, reject) => {
    request.get(`/offers/${id}/forModel`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function createOffer(data) {
  return new Promise((resolve, reject) => {
    request.post('/offers', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function editOffer(data) {
  return new Promise((resolve, reject) => {
    request.put(`/offers/${data.id}`, data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function setEndedOffer(id) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${id}/setEnded`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function setViewedOffer(id) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${id}/setViewed`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function activateOffer(id) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${id}/activate`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function closeOffer(id) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${id}/close`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function updateOffer(offer) {
  return new Promise((resolve, reject) => {
    request.patch(`/offers/${offer.id}/`, offer)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function endOffer(id) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${id}/end`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function sendOfferApplication(offer_id) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${offer_id}/applications`, {status: 'sended'})
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function ignoreOfferApplication(offer_id) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${offer_id}/applications`, {status: 'ignored'})
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function refuseOfferFromModel(linkId) {
  return new Promise((resolve, reject) => {
    request.post(`/offer-links/${linkId}/refuseFromModel`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function agreeOfferFromModel(linkId) {
  return new Promise((resolve, reject) => {
    request.post(`/offer-links/${linkId}/agreeFromModel`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function agreeOfferUser(offerId, userId) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${offerId}/users/${userId}/agree`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}


export async function refuseOfferUser(offerId, userId) {
  return new Promise((resolve, reject) => {
    request.post(`/offers/${offerId}/users/${userId}/refuse`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}


export async function agreeOfferFromManager(linkId) {
  return new Promise((resolve, reject) => {
    request.post(`/offer-links/${linkId}/agreeFromManager`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function refuseOfferFromManager(linkId) {
  return new Promise((resolve, reject) => {
    request.post(`/offer-links/${linkId}/refuseFromManager`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchOfferBill(id) {
  return new Promise((resolve, reject) => {
    request.get(`/offers/${id}/getBill`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function deleteOffer(id) {
  return new Promise((resolve, reject) => {
    request.delete(`/offers/${id}`)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
