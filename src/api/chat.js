import request from '@/utils/request';

export async function fetchDialogs() {
  return new Promise((resolve, reject) => {
    request.get('/messages')
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function sendMessage(data) {
  return new Promise((resolve, reject) => {
    request.post('/messages', data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function sendMessageByDialog(data) {
  return new Promise((resolve, reject) => {
    request.post('/messages/dialog/' + data.dialog_id, data)
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function markAsRead(id) {
  return new Promise((resolve, reject) => {
    request.post('/messages/markAsRead', { id })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function markDialogAsRead(id) {
  return new Promise((resolve, reject) => {
    request.post('/messages/markDialogAsRead', { id })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}

export async function fetchMessages(dialogId) {
  return new Promise((resolve, reject) => {
    request.get('/messages/listDialog', { params: { dialog_id: dialogId } })
      .then((response) => {
        resolve(response.data);
      })
      .catch((exception) => {
        reject(exception);
      });
  });
}
