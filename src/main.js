// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import 'vuetify/dist/vuetify.min.css';
import vuetify from '@/plugins/vuetify';
import './plugins/element';
import './plugins/mask';
import './assets/styles/main.css';
import './permission';
import './plugins/firebase';
import './plugins/lazy';
import './plugins/gallery';
import i18n from './plugins/i18n';
import store from '@/store';

require('dotenv').config();

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  vuetify,
  store,
  i18n,
  components: { App },
  template: '<App/>',
});
