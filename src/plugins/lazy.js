import Vue from 'vue';
import VueLazyload from 'vue-lazyload';

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: '/static/error.png',
  loading: '/static/loading.svg',
  attempt: 10,
});
