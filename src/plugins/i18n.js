import Vue from 'vue';
import VueI18n from 'vue-i18n';
import store from '@/store';

const messages = {};

const locales = require.context('./locales', true, /[a-z0-9]+\.json$/i);
locales.keys().forEach((key) => {
  const locale = key.match(/([a-z0-9]+)\./i)[1];
  messages[locale] = locales(key);
});

Vue.use(VueI18n);

export default new VueI18n({
  locale: store.getters.locale, // set locale
  messages, // set locale messages
});
