import * as firebase from 'firebase';
import store from '@/store';
import { Message } from 'element-ui';

var firebaseConfig = {
  apiKey: ""
};

firebase.initializeApp(firebaseConfig);
window.firebase = firebase;

let messaging;
if (firebase.messaging.isSupported()) {
  messaging = firebase.messaging();
}

// используем localStorage для отметки того,
// что пользователь уже подписался на уведомления
function isTokenSentToServer(currentToken) {
  return window.localStorage.getItem('sentFirebaseMessagingToken') === currentToken;
}

function deleteCurrentToken() {
  const currentToken = window.localStorage.getItem('sentFirebaseMessagingToken');
  if (currentToken) { store.dispatch('user/removePushToken', currentToken); }
}

function setTokenSentToServer(currentToken) {
  window.localStorage.setItem(
    'sentFirebaseMessagingToken',
    currentToken || '',
  );
}

function sendTokenToServer(currentToken) {
  if (!isTokenSentToServer(currentToken)) {
    // удаляем прошлый токен
    deleteCurrentToken();
    // отправляем новый токен
    store.dispatch('user/addPushToken', currentToken);
    setTokenSentToServer(currentToken);
  } else {
    console.log('Токен уже отправлен на сервер.');
  }
}

function getToken() {
  console.log('get token');
  messaging.requestPermission()
    .then(() => {
      messaging.getToken()
        .then((currentToken) => {
          if (currentToken) {
            sendTokenToServer(currentToken);
          } else {
            Message.error('Не удалось подключить уведомления');
            setTokenSentToServer(false);
          }
        })
        .catch((err) => {
          Message.error('Ошибка подключения уведомлений');
          setTokenSentToServer(false);
        });
    })
    .catch((err) => {
      Message.error('Ошибка подключения уведомлений. Вы отказали в разрешении получать уведомления');
    });
}

export function startGetToken() {
  if (firebase.messaging.isSupported()) {
    getToken();
  }
}

if (firebase.messaging.isSupported()) {
// Callback fired if Instance ID token is updated.
  messaging.onTokenRefresh(() => {
    messaging.getToken()
      .then((refreshedToken) => {
        sendTokenToServer(refreshedToken);
      })
      .catch((err) => {
        console.log('Unable to retrieve refreshed token ', err);
      });
  });

  messaging.onMessage((payload) => {
    console.log('Message received. ', payload);
    // ...
  });
}
