import router from './router';
import store from './store';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { Message } from 'element-ui';
import getPageTitle from '@/utils/get-page-title';

NProgress.configure({ showSpinner: false }); // NProgress Configuration

const whiteList = ['/login', '/register', '/auth-token']; // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start();

  // set page title
  document.title = getPageTitle(to.name);
  document.body.classList = `page-${to.name}`;

  if (store.getters.token) {
    if (to.path === '/login') {
      next({ path: '/' });
      NProgress.done();
    } else if (store.getters.loggedUser) {
      next();
      NProgress.done();
    } else {
      try {
        await store.dispatch('user/getInfo');
        next();
        NProgress.done();
      } catch (error) {
        // remove token and go to login page to re-login
        await store.dispatch('user/resetToken');
        Message.error(error || 'Has Error');
        next(`/login?redirect=${to.path}`);
        NProgress.done();
      }
    }
  } else {
    /* has no token */
    if (whiteList.indexOf(to.path) !== -1) {
      next();
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`);
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  // finish progress bar
  NProgress.done();
});
