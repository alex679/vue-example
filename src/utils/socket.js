import SocketIO from 'socket.io-client';
import Echo from 'laravel-echo';

export default new class Socket {
  init(user_id, token, processNotification) {
    this.socket = new Echo({
      broadcaster: 'socket.io',
      client: SocketIO,
      host: process.env.SOCKETIO_HOST,
      auth: { headers: { Authorization: `Bearer ${token}` } },
      headers: { Authorization: `Bearer ${token}` },
    });
    this.xsocket_channel = `App.User.${user_id}`;
    this.registerSocketEventHandlers();
    this.processNotification = processNotification;
  }



  registerSocketEventHandlers() {
    this.socket.private(this.xsocket_channel)
      .notification(notification => this.processNotification(notification))
      .listen('.PrivateMessage', (message_object) => {
        this.processNotification({
          message_object,
          type: 'PrivateMessage',
        });
      })
      .listen('.DialogMessage', (message_object) => {
        this.processNotification({
          message_object,
          type: 'DialogMessage'
        })
      });
  }

  getInstance() {
    return this.socket
  }

  disconnect() {
    if (this.socket) {
      this.socket.leave(this.xsocket_channel);
      this.socket = null;
      this.xsocket_channel = null;
    }
  }
}
