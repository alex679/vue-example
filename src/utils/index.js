import dayjs from 'dayjs';
import store from '@/store';
import 'dayjs/locale/ru';
import 'dayjs/locale/en';

const localizedFormat = require('dayjs/plugin/localizedFormat');

dayjs.locale(store.getters.locale);
dayjs.extend(localizedFormat);

export function formatPostDate(date) {
  return dayjs(date).format('LL');
}

export function formatDate(date) {
  return dayjs(date).format('D MMMM, YYYY');
}

export function copyToClipboard(text) {
  const input = document.createElement('textarea');
  input.innerHTML = text;
  document.body.appendChild(input);
  input.select();
  const result = document.execCommand('copy');
  document.body.removeChild(input);
  return result;
}

export function declOfNum(number, titles) {
  const cases = [2, 0, 1, 1, 1, 2];
  return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

export function dataURLtoBlob(dataURL) {
  const binary = atob(dataURL.split(',')[1]);
  // Create 8-bit unsigned array
  const array = [];
  let i = 0;
  while (i < binary.length) {
    array.push(binary.charCodeAt(i));
    i++;
  }
  // Return our Blob object
  return new Blob([new Uint8Array(array)], { type: 'image/png' });
}
