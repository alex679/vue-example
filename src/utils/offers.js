export function getOfferStatus(status) {
  if (status === 'wait_pay') { return 'Не оплачено'; }
  if (status === 'inspection') { return 'На проверке'; }
  if (status === 'blocked') { return 'Заблокировано'; }
  if (status === 'active') { return 'Активное'; }
  if (status === 'ended') { return 'Завершено'; }
  if (status === 'closed') { return 'Закрыто'; }
  return 'Закрыто';
}

export function getOfferModelStatus(status) {
  if (status === 'wait_model') { return 'Новое'; }
  if (status === 'model_refused') { return 'Вы отказались'; }
  if (status === 'model_agreed') { return 'Ожидание менеджера'; }
  if (status === 'manager_refused') { return 'Вам отказано'; }
  if (status === 'manager_agreed') { return 'Вы приняты'; }
  return 'Новое';
}

export function getOfferModelStatusColor(status) {
  if (status === 'wait_model') { return '#9ACD32'; }
  if (status === 'model_agreed') { return '#9ACD32'; }
  if (status === 'manager_agreed') { return '#9ACD32'; }
  if (status === 'model_refused') { return '#FF6347'; }
  if (status === 'manager_refused') { return '#B22222'; }
  return '#9ACD32';
}

export function getTourStatus(status) {
  if (status === 'wait_pay') { return 'Не оплачен'; }
  if (status === 'inspection') { return 'На проверке'; }
  if (status === 'blocked') { return 'Заблокировано'; }
  if (status === 'active') { return 'Активно'; }
  if (status === 'closed') { return 'Закрыт'; }
  return 'Закрыт';
}

export function getTourStatusColor(status) {
  if (status === 'wait_pay') { return '#9ACD32'; }
  if (status === 'inspection') { return '#aaa'; }
  if (status === 'blocked') { return '#B22222'; }
  if (status === 'active') { return '#9ACD32'; }
  if (status === 'ended') { return '#FF6347'; }
  if (status === 'closed') { return '#DC143C'; }
  return '#DC143C';
}
