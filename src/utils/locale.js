import detectBrowserLanguage from 'detect-browser-language';

const localeKey = 'locale';

function getBrowserLanguage() {
  const locale = detectBrowserLanguage();
  if (locale.indexOf('ru') !== -1) { return 'ru'; }
  return 'en';
}

export function getLocale() {
  return localStorage.getItem(localeKey) || getBrowserLanguage();
}

export function setLocale(locale) {
  localStorage.setItem(localeKey, locale);
}

export function removeLocale() {
  localStorage.removeItem(localeKey);
}
